<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property integer $id
 * @property string $user
 * @property integer $id_rol
 * @property string $clave
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user', 'id_rol', 'clave'], 'required'],
            [['id', 'id_rol'], 'integer'],
            [['user', 'clave'], 'string', 'max' => 80],
            [['user'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user' => 'User',
            'id_rol' => 'Id Rol',
            'clave' => 'Clave',
            'token' => 'token',
        ];
    }
}
