<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentario".
 *
 * @property integer $id
 * @property string $comentario
 * @property integer $id_user
 * @property string $at_created
 * @property string $autor
 * @property string $empresa
 * @property string $at_updated
 */
class Comentario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */            

    public static function tableName()
    {
        return 'comentario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comentario', 'autor', 'empresa'], 'required'],
            [['comentario'], 'string'],
            [['at_created', 'at_updated'], 'safe'],
            [['foto'], 'image',  'extensions' => 'jpg,png,jpeg'],
            [['autor', 'empresa'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comentario' => 'Comentario',
            'id_user' => 'Id User',
            'at_created' => 'At Created',
            'autor' => 'Autor',
            'foto' => 'Foto',
            'empresa' => 'Empresa',
            'at_updated' => 'At Updated',
            'estado' => 'estado',
        ];
    }
}
