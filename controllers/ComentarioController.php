<?php

namespace app\controllers;
header('Access-Control-Allow-Origin: *');            
use Yii;
use app\models\Comentario;
use app\models\ComentarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ComentarioController implements the CRUD actions for Comentario model.
 */
class ComentarioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
[
            'class' => \yii\behaviors\TimestampBehavior::className(),
            'createdAtAttribute' => 'at_created',
            'updatedAtAttribute' => 'at_updated',
            'value' => new \yii\db\Expression('NOW()'),
        ],
        ];
    }

    /**
     * Lists all Comentario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ComentarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $comentarios = Comentario::find()->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'comentarios' => $comentarios,

        ]);
    }

    public function actionTodosweb()
    {   
        $arr = "";  
        $comentarios = Comentario::find()->orderBy(['id'=>SORT_DESC])->all();
        $arr = $arr.'<div class="container"> 
                <div class="testimonials-slider"> 
                    <div class="slides-pagin"></div>
                    <ul class="t-slides center" >  ';
        foreach ($comentarios as $comentario ){
        $arr = $arr.'<li class="monial animated" data-animation="fadeInUp" data-animation-delay="400"><h3 class="monial-title uppercase white">'.$comentario->comentario.'</h3><img src="http://localhost:8080/coment/'. $comentario->foto .'" alt="'.$comentario->autor.'"><p class="author italic">'.$comentario->autor.'</p></li>';  

        }
        $arr = $arr.'</ul>
                </div>
            </div>';
       ////////////// return json_encode(array('data'=>$arr));
        return    $arr  ;
    }

    public function actionTodos()
    {
        $comentarios = Comentario::find()->orderBy(['id'=>SORT_DESC])->all();
        $cant = Comentario::find()->count();
        $dif = $cant - $_GET["cant"];

        $arr = "";
$i = 0;
        foreach ($comentarios as $comentario ) 
        {
            if ($comentario->estado == "0") {
                $cad2 = '<h6><span class="label label-danger">No Publicado</span></h6>'; 
                $cad3 = ' <li>'. \yii\helpers\Html::a('Publicar', ['publicar', 'id' => $comentario->id], [ ]).'</li> '; 
            }else{
                $cad2 = '<h6><span class="label label-success">Publicado</span></h6>'; 
                $cad3 = ' <li>'. \yii\helpers\Html::a('Despublicar', ['despublicar', 'id' => $comentario->id], [ ]).'</li> ';
            }


            $i++;
            if ($dif > 0 && $_GET["cant"] != "-1") {
                $cad = 'class="success"';
                $dif--;
            }else{
                $cad = '';
            }
           
            $arr = $arr. '<tr '.$cad.'>
                        <td>
                            <aside class="contentHeight">
                                <span>'.$comentario->autor.'</span> </aside>
                                                        </td><td><aside class="contentHeight">
                                                                <span>'. $comentario->empresa.'</span>
                                                                                            </aside>
                                                                                        </td>
                                                                                        <td>
                                                                                            <aside class="contentHeight">
                                                                                                <span>'
                        .$comentario->comentario .'</span>
                                                    </aside>
                                                </td>
                                                <td class="text-center" style="width:10%;">
                                                    <img   src="coment/'. $comentario->foto .'" width="100" alt="" class="img-circle">
                                                                            </td>                      
                                <td class="text-center" style="width:10%;">'.$cad2.'</td> 
                                                                            <td style="width:10%;">
                                                                                <aside class="contentHeight text-uppercase">
                                                                                    <div class="btn-group">
                                                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        Acciones <span class="caret"></span>
                                                                                        </button>
                                                                                        <ul class="dropdown-menu "> 


  '.$cad3.'
 
 <li>'. \yii\helpers\Html::a('Ver', ['view', 'id' => $comentario->id], [ ]).'</li> 
 <li>'. \yii\helpers\Html::a('Editar', ['update', 'id' => $comentario->id], [ ]).'</li> 
<li>'.\yii\helpers\Html::a('Borrar', ['delete', 'id' => $comentario->id], [
            'class' => '',
            'data' => [
                'confirm' => 'Desea borrar este item?',
                'method' => 'post',
            ],
        ]).'</li>                                        
                                     </ul>
                                 </div>
                             </aside>
                          </td>                      
                     </tr>';

        }
        return json_encode(array('data'=>$arr, 'cant'=>$i));
    }

    /**
     * Displays a single Comentario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Comentario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Comentario();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if($_FILES["Comentario"]["error"]["foto"] == "0" )
            {
                $model->foto = \yii\web\UploadedFile::getInstance($model, 'foto');            
                $model->foto->saveAs('coment/' . $model->id.'.'.$model->foto->extension);
                $model->foto = $model->id.'.'.$model->foto->extension;
                $model->save();               
            }        
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Comentario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionPublicar($id)
    {
        $model = $this->findModel($id);
        $model->estado = 1; 
        $model->save();
        return $this->redirect(['index']);
    }

    public function actionDespublicar($id)
    {
        $model = $this->findModel($id);
        $model->estado = 0; 
        $model->save();
        return $this->redirect(['index']);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if($_FILES["Comentario"]["error"]["foto"] == "0" )
            {
                @unlink('coment/' . $model->id.'.'.$model->foto->extension);
                $model->foto = \yii\web\UploadedFile::getInstance($model, 'foto');            
                $model->foto->saveAs('coment/' . $model->id.'.'.$model->foto->extension);
                $model->foto = $model->id.'.'.$model->foto->extension;
                $model->save();               
            }  
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Comentario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        @unlink('coment/' . $model->id.'.'.$model->foto->extension);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Comentario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comentario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comentario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
