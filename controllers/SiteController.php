<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['/comentario']);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->params['guest'] = 1;
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionRecovery()
    {
        if (isset($_POST["email"])) {
            $pwd = bin2hex(openssl_random_pseudo_bytes(23)); 
           /* Yii::$app->mailer->compose()
        ->setFrom('info@gmail.com')
        ->setTo($_POST["gmail"])
        ->setSubject('Recuperar clave')
        ->setTextBody('Use este token para recuperar su clave :'.$pwd.'</br>')
        ->send();*/

        $user = \app\models\Usuario::find()
        ->where(['user'=>$_POST["username"]])
        ->one();
        $user->token = $pwd;
        $user->save();
            return $this->redirect(['/site/getpass']);
        }else{
          return $this->render('recovery');
        }
        
    }

    public function actionGetpass()
    {
        if (isset($_POST["token"]) && isset($_POST["clave1"]) && isset($_POST["clave2"]) && $_POST["clave2"]==$_POST["clave1"]) {

            $usern = \app\models\Usuario::find()->Where(['token'=>$_POST["token"]])->count();
            if($usern > 0)
            {
                $user = \app\models\Usuario::find()->Where(['token'=>$_POST["token"]])->one();
                
                if($user->token ==  $_POST["token"])
                {
                    $user->clave = $_POST["clave1"];
                    $user->token = "";
                    $user->save();
                    return $this->redirect(['/site/login']);
                }
            }   
        }
        return $this->render('getpass');
    }
}
