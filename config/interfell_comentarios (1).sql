-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-10-2017 a las 16:11:54
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `interfell_comentarios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE IF NOT EXISTS `comentario` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `at_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `autor` varchar(150) NOT NULL,
  `empresa` varchar(150) NOT NULL,
  `at_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comentario` text NOT NULL,
  `foto` varchar(256) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`id`, `id_user`, `at_created`, `autor`, `empresa`, `at_updated`, `comentario`, `foto`) VALUES
(8, NULL, '2017-10-02 21:48:58', 'ffffffffffff', 'fffffffff', '0000-00-00 00:00:00', 'ffffffffffffffffffff', NULL),
(9, NULL, '2017-10-02 21:50:38', 'dadddddd2355', 'ddddddddddd', '0000-00-00 00:00:00', 'ddddddddddddd', ''),
(10, NULL, '2017-10-02 21:54:10', 'ddddddd', 'dddddddddddddddddd', '0000-00-00 00:00:00', 'ddddddddddd', NULL),
(11, NULL, '2017-10-02 21:59:43', 'ggggggg', 'gggggg', '0000-00-00 00:00:00', 'gggggg', NULL),
(12, NULL, '2017-10-02 22:06:22', 'ffffffffffffff', 'fffffff', '0000-00-00 00:00:00', 'ffffffffffff', ''),
(13, NULL, '2017-10-02 22:06:29', 'ffffffffffffff', 'fffffff', '0000-00-00 00:00:00', 'ffffffffffff', 'jpg'),
(14, NULL, '2017-10-02 22:08:17', 'ffffffffffffff', 'fffffff', '0000-00-00 00:00:00', 'ffffffffffff', '14.jpg'),
(15, NULL, '2017-10-02 22:08:34', 'ffffffffffffff', 'fffffff', '0000-00-00 00:00:00', 'ffffffffffff', '15.jpg'),
(16, NULL, '2017-10-03 02:56:24', 'dsads', 'sad', '0000-00-00 00:00:00', 'sadddddddddddddd', ''),
(17, NULL, '2017-10-03 04:58:35', 'ddddddddddd', 'dddddddddddddd', '0000-00-00 00:00:00', 'dddddddddd', ''),
(18, NULL, '2017-10-03 04:59:00', 'aaaaaaaaaaaaaa', 'aaaaaaaaaa', '0000-00-00 00:00:00', 'ddsaaaaaaaaaa', ''),
(22, NULL, '2017-10-03 05:25:34', 'tttt', 'jjj', '0000-00-00 00:00:00', 'mmmm', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE IF NOT EXISTS `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL,
  `user` varchar(80) NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
