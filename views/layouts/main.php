<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <?php if (
    (Yii::$app->controller->action->id == 'login' && Yii::$app->controller->id == 'site')
        || 
    (Yii::$app->controller->action->id == 'recovery' && Yii::$app->controller->id == 'site') 
        || 
    (Yii::$app->controller->action->id == 'getpass' && Yii::$app->controller->id == 'site')   
    ) { ?>
    <style>body{padding-top:40px;padding-bottom:40px;background-color:#eee}.form-signin{max-width:330px;padding:15px;margin:0 auto}.form-signin .checkbox,.form-signin .form-signin-heading{margin-bottom:10px}.form-signin .checkbox{font-weight:400}.form-signin .form-control{position:relative;height:auto;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:10px;font-size:16px}.form-signin .form-control:focus{z-index:2}.form-signin input[type=email]{margin-bottom:-1px;border-bottom-right-radius:0;border-bottom-left-radius:0}.form-signin input[type=password]{margin-bottom:10px;border-top-left-radius:0;border-top-right-radius:0}</style>
    <?php $out = "0"; }else{ $out = "1";} ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
     if ($out == "1" ) {
        NavBar::begin([
            'brandLabel' => 'Dashboard',
            'brandUrl' => ['/comentario'],
            'options' => [
                'class' => 'navbar-default navbar-fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Crear comentario', 'url' => ['/comentario/create']],
                Yii::$app->params['guest']  ? (
                    ['label' => 'Login', 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Logout ',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
        NavBar::end();
    }
    ?>

    <div class="container">
<?php if ($out == "1" ) { ?>
        <?= Breadcrumbs::widget([
            'homeLink' => ['label' => 'Home','url' => 'index.php?r=comentario/index'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?php } ?>
        <?= $content ?>
    </div>
</div>

<?php if ($out == "1" ) { ?>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
<?php } ?>
<?php $this->endBody();   ?>
</body>
<script src="/assets/js/datatables.min.js"></script>
<script>
            $(document).ready(function() {                          
                    $('#listadoGeneral').DataTable( {
                        language: {
                            "sProcessing":     "Procesando...",
                            "sLengthMenu":     "Mostrar _MENU_ registros",
                            "sZeroRecords":    "No se encontraron resultados",
                            "sEmptyTable":     "Ningún dato disponible en esta tabla",
                            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix":    "",
                            "sSearch":         "Buscar:",
                            "sUrl":            "",
                            "sInfoThousands":  ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst":    "Primero",
                                "sLast":     "Último",
                                "sNext":     "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        }
                } );

            
            } );
            var cant = -1;
            setInterval(function(){ 
                $.ajax({
                    method: "GET",
                    url: "http://localhost:8080/index.php?r=comentario/todos",
                    data:{cant:cant} ,
                    success:function(data){
                        var json = $.parseJSON(data);
                        if(cant == json['cant'])
                        {

                        }else{
                            $("#in").html(json['data']); 
                            cant = json['cant'];
                        }
                     
                    }   
                });
            }, 1000);
                      

        </script>
</html>
<?php $this->endPage() ?>
