<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
  

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        
        <section class="container">
        <div class="col-lg-4 col-lg-offset-4">
            <form class="form-signin">
                <h2 class="form-signin-heading text-uppercase">Inicia sesión</h2>
                <label for="inputEmail" class="sr-only">Username</label>
                <input autocomplete="off" type="text" id="loginform-username" class="form-control" name="LoginForm[username]"   placeholder="Username" required autofocus>
                <label for="inputPassword" class="sr-only">Clave</label>
                <input type="password" id="loginform-password" class="form-control" name="LoginForm[password]" placeholder="Clave" required>
                <?= \yii\helpers\Html::a('Recuperar clave', ['recovery'], ['class' => 'pull-right']) ?>
                <hr>
                <?= \yii\helpers\Html::submitButton('Ingresar' , ['class' =>  'btn btn-lg btn-primary btn-block']); ?>
            </form>
            </div>
        </section> <!-- /container -->



    <?php ActiveForm::end(); ?>
 