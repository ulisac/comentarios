<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
  

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        
        <section class="container">
        <div class="col-lg-4 col-lg-offset-4">
            <form class="form-signin">
                <h2 class="form-signin-heading text-uppercase">Recuperar clave</h2>
                <label for="inputEmail" class="sr-only">Username</label>
                <input type="input" name="username" class="form-control" placeholder="Username" required autofocus>   
                <label for="inputEmail" class="sr-only">Correo electrónico</label>
                <input type="email" name="email" class="form-control" placeholder="Correo electrónico" required autofocus> 
                <hr>                                
                <!-- <button class="btn btn-lg btn-success btn-block" type="submit">Enviar</button> -->

                <!-- USAR SUBMIT -->

                <?= \yii\helpers\Html::submitButton('Enviar' , ['class' =>  'btn btn-lg btn-success btn-block']); ?>
                <?= \yii\helpers\Html::a('Atras', ['/site/login'], ['class' => 'btn btn-lg btn-primary btn-block']) ?>
            </form>
            </div>
        </section> <!-- /container -->


    <?php ActiveForm::end(); ?>
 