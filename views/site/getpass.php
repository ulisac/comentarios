<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
  

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        
        <section class="container">
        <div class="col-lg-4 col-lg-offset-4">
            <form class="form-signin">
                <h2 class="form-signin-heading text-uppercase">Recuperar Clave</h2>
                <label for="inputPassword1" class="sr-only">Ingrese su nueva clave</label>
                <input type="password" name="clave1"  class="form-control signin" placeholder="Ingrese su nueva clave" required autofocus>
                <label for="inputPassword2" class="sr-only">Clave</label>
                <input type="password" name="clave2" class="form-control" placeholder="Valide su clave" required>   
                <label for="inputPassword2" class="sr-only">Toke</label>
                <input type="text" autocomplete="off" name="token" class="form-control" placeholder="Token de acceso" required>   
                <p class="help-block">Su nueva clave debe contener al menos 8 caracteres.</p>       
                <hr>
                <!-- <button class="btn btn-lg btn-success btn-block" type="submit">Cambiar clave</button> -->
                <?= \yii\helpers\Html::submitButton('Enviar' , ['class' =>  'btn btn-lg btn-primary btn-block']); ?>
            </form>
            </div>
        </section> <!-- /container -->


    <?php ActiveForm::end(); ?>