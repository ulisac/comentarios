<?php

use yii\helpers\Html;
use yii\grid\GridView;     
$this->registerJsFile("/assets/js/datatables.min.js");
     
$this->registerCssFile("/assets/css/datatables.css");
/* @var $this yii\web\View */
/* @var $searchModel app\models\ComentarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comentarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comentario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>







<table id="listadoGeneral" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Autor</th>
                        <th>Empresa</th>
                        <th>Mensaje</th>
                        <th>Foto</th>                       
                        <th>Estado</th>                       
                        <th>Opciones</th>                       
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Autor</th>
                        <th>Empresa</th>
                        <th>Mensaje</th>
                        <th>Foto</th>   
                        <th>Estado</th>  
                        <th>Opciones</th>   
                    </tr>
                </tfoot>
                <tbody id="in">    
                    
                </tbody>
            </table>
    <p>
        <?= Html::a('Crear Comentario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
       
</div>
 