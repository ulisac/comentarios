<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Comentario */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Comentarios', 'url' => ['/comentario']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comentario-view">

    <h1><?= Html::encode($this->title) ?></h1>
<p>
   <img onerror="this.src='assets/images/7x.jpg'" src="coment/<?= $model->foto ?>" width="100" alt="" class="img-circle">
</p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id', 
            'at_created',
            'autor',
            'empresa',
            'at_updated',
            'comentario:ntext',
        ],
    ]) ?>

 
        <?= \yii\helpers\Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Desea borrar este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
