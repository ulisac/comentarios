<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; 
/* @var $this yii\web\View */
/* @var $model app\models\Comentario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comentario-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <?= $form->field($model, 'autor')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <?= $form->field($model, 'empresa')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <?= $form->field($model, 'comentario')->textarea(['rows' => 6]) ?>
                        </div>                  
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  
                            <?= $form->field($model, 'foto')->fileInput(['class' => 'form-control', 'accept' => 'image/*']) ?>
                              
                    </div>
                </div>  

    <div class="form-group"> 
  <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Editar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
<?php if(
(Yii::$app->controller->action->id == 'update' && Yii::$app->controller->id == 'comentario')
    ){ 
       
         echo  Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Desea borrar este item?',
                'method' => 'post',
            ],
        ]);
            } ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
